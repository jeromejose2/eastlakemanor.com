<?php include('./includes/top.php');
?>


	<style type="text/css">

a.hover {
	color: red;
}

#demo-list {
	padding: 0;
	list-style: none;
	margin: 0;
}

#demo-list .file-invalid {
	cursor: pointer;
	color: #514721;
	padding-left: 48px;
	line-height: 24px;
	/*
	//change with error icon
	background: url(assets/error.png) no-repeat 24px 5px; */
	margin-bottom: 1px;
}
#demo-list .file-invalid span {
	background-color: #fff6bf;
	padding: 1px;
}

#demo-list .file {
	line-height: 2em;
	padding-left: 22px;
	/*
	//change with file icon
	background: url(assets/attach.png) no-repeat 1px 50%; */
}

#demo-list .file span,
#demo-list .file a {
	padding: 0 4px;
}

#demo-list .file .file-size {
	color: #666;
}

#demo-list .file .file-error {
	color: #8a1f11;
}

#demo-list .file .file-progress {
	width: 125px;
	height: 12px;
	vertical-align: middle;
	background-image: url(./progress.gif);
}

	</style>



	<script type="text/javascript" src="./mootools.js"></script>
	<script type="text/javascript" src="./Fx.ProgressBar.js"></script>
	<script type="text/javascript" src="./Swiff.Uploader.js"></script>
	<script type="text/javascript" src="./FancyUpload3.Attach.js"></script>
	<script type="text/javascript">

/**
 * FancyUpload Showcase
 *
 * @license		MIT License
 * @author		Harald Kirschner <mail [at] digitarald [dot] de>
 * @copyright	Authors
 */

window.addEvent('domready', function() {

	/**
	 * Uploader instance
	 */
	var up = new FancyUpload3.Attach('demo-list', '#demo-attach, #demo-attach-2', {
		path: './Swiff.Uploader.swf',
		url: './script.php',
		
		/* front side validation */
		fileSizeMax: 20 * 1024 * 1024,
		
		typeFilter: {
			'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
		},
		
		

		
		verbose: true,
		
		onSelectFail: function(files) {
			files.each(function(file) {
				new Element('li', {
					'class': 'file-invalid',
					events: {
						click: function() {
							this.destroy();
						}
					}
				}).adopt(
					new Element('span', {html: file.validationErrorMessage || file.validationError})
				).inject(this.list, 'bottom');
			}, this);	
		},
		
		onFileSuccess: function(file) {
			new Element('input', {type: 'checkbox', 'checked': true}).inject(file.ui.element, 'top');
			file.ui.element.highlight('#e6efc2');
			
document.getElementById('galleryID').src = './gallery.php';


		},
		
		onFileError: function(file) {
			file.ui.cancel.set('html', 'Retry').removeEvents().addEvent('click', function() {
				file.requeue();
				return false;
			});
			
			new Element('span', {
				html: file.errorMessage,
				'class': 'file-error'
			}).inject(file.ui.cancel, 'after');
		},
		
		onFileRequeue: function(file) {
			file.ui.element.getElement('.file-error').destroy();
			
			file.ui.cancel.set('html', 'Cancel').removeEvents().addEvent('click', function() {
				file.remove();
				return false;
			});
			
			this.start();
		}
		
	});

});

		/* ]]> */
	</script>




	<iframe src="gallery.php" id="galleryID" style="width: 510px; height: 600px; border-style:dotted; border-width:1px; margin-left:10px; "></iframe>
		<div style="width: 370px; float: right; text-align: left;">
        	<a href="#" id="demo-attach">Attach a file</a>
        <ul id="demo-list"></ul>
		</div>

<?php include('./includes/bottom.php');





