<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>East Lake Manor (Assisted Living Home)</title>
<meta name="description" content="Welcome to East Lake Manor- Southern Florida's premier Assisted Living Facility. ELM is an owner-operated home that features a tender, hands-on approach to caregiving." />
<meta name="keywords" content="east lake manor, assisted living home, caregiving, community, florida, tarpon springs" />
<meta name="author" content="Tomy" />
<meta http-equiv="expires" content="-1" />
<meta http-equiv= "pragma" content="no-cache" />
<meta name="robots" content="all" />
<meta name="resource-type" content="document" /> 
<meta name="verify-v1" content="lB8N/BRABvIpQO99YrGA9NorX+szcw0ePBJtULLz3CA=" />
<meta name="google-site-verification" content="7lEx5n-QJTJu0o6s9ZH4y2tiUdS2SR_Id84bZFmB5Qc" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="shortcut icon" href="images/favicon.ico" />
<script type="text/javascript" src="js/afn.js"></script>
<script type="text/javascript" src="js/pnghack.js"></script>
<script type="text/javascript" src="js/colorbox/jquery.min.js"></script>
<script type="text/javascript" src="js/colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="js/colorbox/colorbox-1.js"></script>
<script type="text/javascript">
	ph = PNGHack('images/spacer.gif');
	window.onload = function() {
		ph.hackClass();
	};
</script>
</head>

<body>
<!-- Top -->
<div class="top-wrap re">
	<!-- Top -->
	<div class="top re">
		<h1><a href="index.php">East Lake Manor - Assisted Living Home</a></h1>
		<!-- Top Right -->
		<div class="top-right-body re fr">
			<a class="infomail ab" href="mailto:info@eastlakemanor.com">info@eastlakemanor.com</a>
			
			<!-- AddThis Button BEGIN -->
			<div class="share-holder ab">				
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<a class="addthis_button" onmouseover="return addthis_open(this, '', 'www.eastlakemanor.com', 'East Lake Manor (Assisted Living Home)')" href="http://www.addthis.com/bookmark.php?v=250&amp;username=lincaotun"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=lincaotun"></script>				
			</div>
			<!-- AddThis Button END -->
			
			<div class="icons-holder ab">
				<a class="home-icon textindent fl" href="index.php"  title="back to home">back to home</a>
				<a class="email-icon textindent fl" href="mailto:info@eastlakemanor.com" title="email">email us</a>
				<a class="facebook-icon textindent fl" href="#" title="follow us on facebook">follow us on facebook</a>
			<br class="clear" />
			</div>
		</div>
		<!-- Top Right -->
	<br class="clear" />
	</div>
	<!-- Top -->	
</div>
<!-- Top -->

<!-- Header Wrap -->
<div class="header-wrap re">
	<!-- Header -->
	<div class="header re">
		<!-- Image Big -->
		<div class="header-img-b re fl">
			<img src="images/main-img.jpg" alt="image name" />
			<span class="header-flo-r ab"><img class="hack-png" src="images/flwr_ryt.png" alt="flower" /></span>
			<span class="header-flo-l ab"><img class="hack-png" src="images/flwr_left.png" alt="flower" /></span>
		</div>
		<!-- Image Big -->
		<!-- Main Navigation -->
		<div class="header-right-body re fl">
			<ul class="mainnav">
				<li><a href="index.php">home</a></li>
				<li><a href="about-us.php">about us</a></li>
				<li><a href="service-offered.php">service offered</a></li>
				<li><a href="facilities.php" class="selected">facilities (photo gallery)</a></li>
				<li><a href="contact-us.php">contact us</a></li>
			</ul>
		</div>
		<!-- Main Navigation -->
	<br class="clear" />
	</div>
	<!-- Header -->
</div>
<!-- Header Wrap -->

<!-- Content Wrap -->
<div class="content-wrap re">
	<!-- Content -->
	<div class="content re">
		<!-- Left Rail -->
		<div class="left-rail re fl">
			<!-- Sub Images -->
			<div class="sub-images re">
				<img class="fl" src="images/sub-img1.jpg" alt="sub image 1" />
				<img class="fr" src="images/sub-img2.jpg" alt="sub image 2" />
			<br class="clear" />
			</div>
			<!-- Sub Images -->
			<!-- Sub Image Description -->
			<div class="sub-img-description re">
				We feature an intimate, state of the art, full-service facility set in the serene East Lake neighborhood- close to Clearwater Beach, various shopping malls, dozens of golf courses, and the City of Tampa.			
			</div>
			<!-- Sub Image Description -->
			<span class="left-bottom-design re"><!-- Left Curve Design --></span>
		</div>
		<!-- Left Rail -->
		<!-- Right Rail -->
		<div class="right-rail re fl">
			<!-- Right Body -->
			<div class="right-body-facilities re">
				<h2>Facilities</h2>
				<p><?php
				include('./config/mysql_connect.php');
				
				$sqlText = "select * from text where section = 'Facilities'";
				$resultText = mysql_query( $sqlText );
				$rowText = mysql_fetch_assoc( $resultText );
				
				echo stripslashes( $rowText['text'] );
				?>
                </p>
				
				<!-- Facilities Images -->
				<div class="facilities-images re">
                
                <?php
				
			include('./config/mysql_connect.php');
	
			$sql = "select * from gallery order by sort ASC";
			$result = mysql_query( $sql );
			
			while( $row = mysql_fetch_assoc( $result ) )
			{
		

	?>

                
					<a href="<?php echo './cms/uploads/' . stripslashes( $row['path'] ); ?>" rel="pop_up" title="Facilities"><img src="<?php echo './cms/uploads/120x90_' . stripslashes( $row['path'] ); ?>" alt="facilities" /></a>
					
<?php			
            }

?>
            
            	<br class="clear" />
				</div>
				<!-- Facilities Images -->
				
			<span class="rb-shadow-tr ab"><!-- Right Body Shadow Top Right --></span>
			<span class="rb-border-tr ab"><!-- Right Body Border Top Right --></span>
			</div>
			<!-- Right Body -->
		</div>
		<!-- Right Rail -->
	<br class="clear" />
	</div>
	<!-- Content -->
</div>
<!-- Content Wrap -->

<!-- Footer Wrap -->
<div class="footer-wrap re">
	<!-- Footer -->
	<div class="footer re">
		<p class="textalign-right">Copyright 2010. East Lake Manor. All Rights Reserved.</p>
	</div>
	<!-- Footer -->
</div>
<!-- Footer Wrap -->

<!--Google Analytics-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7582993-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
